Welcome to _Coin Lock_, a crypto curriencies offline wallet app for your Android device!

This project was initially forking from schildbach's bitcoin wallet project.

This project contains several sub-projects:

 * __wallet__:
     The Android app itself. This is probably what you're searching for.
 * __market__:
     App description and promo material for the Google Play app store.
 * __integration-android__:
     A tiny library for integrating Coin Lock into your own Android app
     (e.g. donations, in-app purchases).
 * __sample-integration-android__:
     A minimal example app to demonstrate integration of Coin Lock into
     your Android app.

You can build all sub-projects at once using Maven:

`mvn clean install`
